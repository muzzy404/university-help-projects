package GraphTheory;

import java.util.Arrays;

/**
 * Created by Daria on 25.04.2017.
 */

public class ShortestPathsAlgorithms {

    static final int INF = 9999;

    static int[][] FloydWarshallAlgorithm(final int graph[][]) {
        final int N = graph[0].length;
        int W[][] = graph.clone();

        for (int k = 0; k < N; ++k) { // for k = 1 to n
            for (int i = 0; i < N; ++i) { // for i = 1 to n
                for (int j = 0; j < N; ++j) { // for j = 1 to n

                    // W[i][j] = min(W[i][j], W[i][k] + W[k][j])
                    if (W[i][j] > W[i][k] + W[k][j])
                        W[i][j] = W[i][k] + W[k][j];

                }
            }
        } // end loops

        return W;
    }

    static int[] DijkstraAlgorithm(final int graph[][], final int src) {
        final int N = graph[0].length;

        int dist[] = new int[N];
        boolean flag[] = new boolean[N];

        Arrays.fill(dist, Integer.MAX_VALUE);
        Arrays.fill(flag, false);

        dist[src] = 0;
        for (int i = 0; i < (N - 1); ++i) {

            int u = minDistanceIndex(dist, flag);
            flag[u] = true;

            // update
            for (int v = 0; v < N; ++v) {
                // Update dist[v] only if is flag is false, there is an
                // edge from u to v, and total weight of path from src to
                // v through u is smaller than current value of dist[v]
                if ((!flag[v])
                 && (graph[u][v] != 0)
                 && (dist[u] != Integer.MAX_VALUE)
                 && (dist[u] + graph[u][v] < dist[v])) {
                    dist[v] = dist[u] + graph[u][v];
                }

            }
        }

        return dist;
    }

    static int[] PrimMST(final int graph[][]) {
        final int N = graph[0].length;

        int MST[] = new int[N];            // Minimum Spanning Tree
        int weight[] = new int[N];         // Weights of nodes
        boolean mstSet[] = new boolean[N];

        Arrays.fill(weight, Integer.MAX_VALUE);
        Arrays.fill(mstSet, false);

        weight[0] = 0; // Fist vertex is a start => weight = 0
        MST[0] = -1;    // First node is always root of MST

        for(int i = 0; i < N; ++i) {

            int u = minDistanceIndex(weight, mstSet);
            mstSet[u] = true;

            // Update key value and parent index of the adjacent
            // vertices of the picked vertex. Consider only those
            // vertices which are not yet included in MST
            for(int v = 0; v < N; ++v) {
                // graph[u][v] is non zero only for adjacent vertices of m
                // mstSet[v] is false for vertices not yet included in MST
                // Update the weight only if graph[u][v] is smaller than weight[v]
                if ((graph[u][v] != 0)
                 && (graph[u][v] < weight[v])
                 && (!mstSet[v])) {
                    MST[v] = u;
                    weight[v] = graph[u][v];
                }
            }

        }

        return MST;
    }

    static public void printMST(final int MST[], final int graph[][]) {
        for(int i = 1; i < MST.length; ++i) {
            System.out.println((MST[i] + 1) + " ---> " + (i + 1) +
                               " weight: " + graph[i][MST[i]]);
        }
    }

    static private int minDistanceIndex(final int dist[], final boolean flag[]) {

        int min = Integer.MAX_VALUE, minIndex = -1;

        for(int i = 0; i < dist.length; ++i) {
            if (!flag[i] && (dist[i] <= min)) {
                min = dist[i];
                minIndex = i;
            }
        }

        return minIndex;
    }

    static void printMatrix(final int matrix[][]) {

        for(int[] row : matrix) {
            for(int i : row) {

                if (i == INF)
                    System.out.print(" INF");
                else
                    System.out.printf("%4d", i);
                System.out.print("|");
            }
            System.out.println();

        } // end printing

    }

}

package GraphTheory;

import java.util.Arrays;

/**
 * Created by Daria on 25.04.2017.
 */

public class Main {

    static final int INF = ShortestPathsAlgorithms.INF;
    static final int from = 1, to = 4;

    // TEST DATA
    static final int testData[][] = {
            {  0, INF,   8,   3, INF},
            {INF,   0,   4,  10,   1},
            {  8,   4,   0, INF,   7},
            {  3,  10, INF,   0, INF},
            {INF,   1,   7, INF,   0}};
    static final int testMST[][] = {
            {  0,   3,   7, INF},
            {  3,   0,   5,   4},
            {  7,   5,   0,   1},
            {INF,   4,   1,   0}};

    // MY TASK DATA
    static final int var6Data[][] = {
            // 1,   2,   3,   4,   5,   6,   7,   8
            {  0,  32, INF, INF, INF, INF, INF, INF},  // 1
            { 32,   0, INF, INF,   6, INF, INF,  28},  // 2
            {INF, INF,   0,  13, INF,   8,  19,   9},  // 3
            {INF, INF,  13,   0, INF, INF, INF,  20},  // 4
            {INF,   6, INF, INF,   0, INF,   2,  18},  // 5
            {INF, INF,   8, INF, INF,   0,  27,  23},  // 6
            {INF, INF,  19, INF,   2,  27,   0,   5},  // 7
            {INF,  28,   9,  20,  18,  23,   5,   0}}; // 8


    public static void main(String[] args) {

//        checkFloydWarshallAlgorithm();
//        System.out.println();
//        checkPrimMST();
//        System.out.println();

        System.out.println("Variant 6 data:");
        ShortestPathsAlgorithms.printMatrix(var6Data);
        final int[][] var6Result = ShortestPathsAlgorithms.FloydWarshallAlgorithm(var6Data);
        System.out.println("\nResult for variant 6 data:");
        ShortestPathsAlgorithms.printMatrix(var6Result);

        System.out.println();
        if (Arrays.asList(var6Result).contains(INF))
            System.out.println("This graph is not connected");
        else
            System.out.println("This graph is connected");

        System.out.printf("The shortest way by Floyd-Warshall algorithm v[%d] ---> v[%d]: %d%n",
                from, to, var6Result[from - 1][to - 1]);

        // find a way from |from| ---> to any node
        int dijkstraResult[] =  ShortestPathsAlgorithms.DijkstraAlgorithm(var6Data, from - 1);
        System.out.printf("The shortest way by Dijkstra algorithm       v[%d] ---> v[%d]: %d%n",
                from, to, dijkstraResult[to - 1]);

        System.out.println("\nMinimum Spanning Tree:");
        ShortestPathsAlgorithms.printMST(ShortestPathsAlgorithms.PrimMST(var6Data), var6Data);
    }

    private static void checkFloydWarshallAlgorithm() {
        System.out.println("W matrix for test data:");
        ShortestPathsAlgorithms.printMatrix(testData);
        System.out.println("\nResult for test data:");
        ShortestPathsAlgorithms.printMatrix(ShortestPathsAlgorithms.FloydWarshallAlgorithm(testData));
    }

    private static void checkPrimMST() {
        System.out.println("Matrix for test Prim’s Minimum Spanning Tree:");
        ShortestPathsAlgorithms.printMatrix(testMST);
        System.out.println("\nResult");
        ShortestPathsAlgorithms.printMST(ShortestPathsAlgorithms.PrimMST(testMST), testMST);
    }

}
